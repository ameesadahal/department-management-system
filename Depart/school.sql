-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2019 at 06:26 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AID` int(11) NOT NULL,
  `ANAME` varchar(150) NOT NULL,
  `APASS` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AID`, `ANAME`, `APASS`) VALUES
(1, 'admin', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `Room_no` int(3) NOT NULL,
  `C_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`Room_no`, `C_type`) VALUES
(23, 'Non Faculty room');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `ID` int(11) NOT NULL,
  `ETYPE` varchar(150) NOT NULL,
  `EDATE` varchar(150) NOT NULL,
  `TIME` int(6) NOT NULL,
  `PROGRAM` varchar(150) NOT NULL,
  `SEM` varchar(150) NOT NULL,
  `SUB` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`ID`, `ETYPE`, `EDATE`, `TIME`, `PROGRAM`, `SEM`, `SUB`) VALUES
(12, 'INTERNAL', '12-07-2019', 20, 'ME', '2', 'Saugat Shakya');

-- --------------------------------------------------------

--
-- Table structure for table `hclass`
--

CREATE TABLE `hclass` (
  `Program` varchar(30) NOT NULL,
  `Sem` int(2) NOT NULL,
  `Sub` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hclass`
--

INSERT INTO `hclass` (`Program`, `Sem`, `Sub`) VALUES
('', 0, 'Saugat Shakya'),
('', 0, 'Saugat Shakya'),
('ME', 2, 'Saugat Shakya');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `name` char(10) NOT NULL,
  `semesters` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`name`, `semesters`) VALUES
('ME', 4),
('MTech', 4);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(150) NOT NULL,
  `GENDER` varchar(8) NOT NULL,
  `TYPE` varchar(150) NOT NULL,
  `QUAL` varchar(150) NOT NULL,
  `POS` varchar(30) NOT NULL,
  `SAL` int(150) NOT NULL,
  `PNO` bigint(150) NOT NULL,
  `MAIL` varchar(150) NOT NULL,
  `ADDR` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `NAME`, `GENDER`, `TYPE`, `QUAL`, `POS`, `SAL`, `PNO`, `MAIL`, `ADDR`) VALUES
(1, 'roman', '', 'Non-Faculty', 'mba', '', 2000, 4567889, 'sdf@gm.com', 'xcvb'),
(123, 'ery', 'Male', 'Faculty', 'mba', 'sweer', 10000, 9843910699, 'xyz@gh.com', 'sdfg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `r_no` int(11) NOT NULL,
  `name` char(30) NOT NULL,
  `age` int(2) NOT NULL,
  `DOB` varchar(150) NOT NULL,
  `GEN` varchar(8) NOT NULL,
  `PHO` bigint(10) NOT NULL,
  `MAIL` varchar(30) NOT NULL,
  `ADDR` text NOT NULL,
  `p_name` varchar(20) NOT NULL,
  `semester` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`r_no`, `name`, `age`, `DOB`, `GEN`, `PHO`, `MAIL`, `ADDR`, `p_name`, `semester`) VALUES
(11111, 'saugat', 20, '15-03-1999', 'Male', 9843910699, 'saugatsakhya@gmail.com', 'ku', 'ME', 2),
(22222, 'amisha', 19, '31-01-2001', 'Female', 9866557807, 'ameesadahal@gmail.com', 'ku', 'ME', 2),
(123456, 'sad', 0, '4-06-1905', 'Male', 1234567, 'asdf@gah.com', 'sdfgh', 'MTech', 3),
(222334, 'erty', 19, '10-08-1915', 'Male', 12345689, 'asr@hu.com', 'asdf', 'MTech', 2),
(1234567, 'roman', 15, '12-03-1999', 'Male', 123456, 'roman@gmail.com', 'asg', 'ME', 4),
(2345678, 'romu', 19, '11-12-1997', 'Male', 9810479710, 'sam@gm.com', 'dfgh', 'MTech', 1),
(3333333, 'prabesh', 20, '10-07-1999', 'Male', 3456778, 'abiabcbi@gmail.com', 'dfgh', 'ME', 2),
(12344567, 'drty', 19, '19-11-2009', 'Male', 9805375020, 'spr@gmail.com', 'dsfg', 'MTech', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sub`
--

CREATE TABLE `sub` (
  `sub_code` int(3) NOT NULL,
  `sub_name` varchar(25) NOT NULL,
  `sub_credit` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub`
--

INSERT INTO `sub` (`sub_code`, `sub_name`, `sub_credit`) VALUES
(110, 'Saugat Shakya', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`Room_no`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`r_no`);

--
-- Indexes for table `sub`
--
ALTER TABLE `sub`
  ADD PRIMARY KEY (`sub_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
