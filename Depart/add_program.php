<?php
	include"database.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
		echo"<script>window.open('index.php?mes=Access Denied...','_self');</script>";
		
	}	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Program</title>			
		<link rel="stylesheet" type="text/css" href="css/styled.css">
	</head>
	<body>
		<?php include"navbar.php";?><br>
		<img src="img/back.jpg" style="margin-left:90px;" class="sha">
			<div id="section">
				<?php include"sidebar.php";?><br>
				<h3 class="text">Welcome <?php echo $_SESSION["ANAME"]; ?></h3><br><hr><br>
				<div class="content1">
					
						<h3 > Add Program Details</h3><br>
					<?php
						if(isset($_POST["submit"]))
						{
							 $sq="insert into program (name,semesters) values('{$_POST["name"]}','{$_POST["semesters"]}')";
							if($db->query($sq))
							{
								echo "<div class='success'>Insert Success..</div>";
							}
							else
							{
								echo "<div class='error'>Insert failed..</div>";
							}
							
							
						}
					
					?>
						
				<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
					<label>Name:</label><br>
                    <input type="text" name="name" class="input" required>
						
					<br><br>
					<label>Semesters</label><br>
					<input type="number" name="semesters" class="input" required>
					<br>
					<button type="submit" class="btn" name="submit">Add Program Details</button>
				</form>
				
				
				</div>
				
				
				<div class="tbox">
					<h3 style="margin-top:30px;"> Program Details</h3><br>
					<?php
						if(isset($_GET["mes"]))
						{
							echo"<div class='error'>{$_GET["mes"]}</div>";	
						}
					
					?>
					<table border="1px" >
						<tr>
							<th>Program name</th>
							<th>Semesters</th>						</tr>
						<?php
							$s="select * from program";
							$res=$db->query($s);
							if($res->num_rows>0)
							{
								while($r=$res->fetch_assoc())
								{
									echo "
										<tr>
											<td>{$r["name"]}</td>
											<td>{$r["semesters"]}</td>
										</tr>
										";
									
								}
								
							}
						?>
					
					</table>
				</div>
			</div>
	
	</body>
</html>