<?php
	include"database.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
		echo"<script>window.open('index.php?mes=Access Denied...','_self');</script>";
		
	}	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Subject</title>
		<link rel="stylesheet" type="text/css" href="css/styled.css">
	</head>
	<body>
				<?php include"navbar.php";?><br>
				<img src="img/bb.jpg" style="margin-left:90px;" class="sha">
				
			<div id="section">
					<?php include"sidebar.php";?><br><br><br>
					<h3 class="text">Welcome <?php echo $_SESSION["ANAME"]; ?></h3><br><hr><br>
					<div class="content1">
					
						<h3 > Add Subject Details</h3><br>
						<?php
							if(isset($_POST["submit"]))
							{
								$sq="insert into sub(sub_code,sub_name,sub_credit) values ('{$_POST["sub_code"]}','{$_POST["sub_name"]}','{$_POST["sub_credit"]}')";
								if($db->query($sq))
								{
									echo "<div class='success'>Insert Success..</div>";
								}
								else
								{
									echo "<div class='error'>Insert Failed..</div>";
								}
							}
						?>
						
						<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
						   <label>Subject Code</label><br>
						   <input type="number" name="sub_code" required class="input"><br>
						   <label>Subject Name</label><br>
						   <input type="text" name="sub_name" required class="input"><br>
						   <label>Subject Credit</label><br>
						   <input type="number" name="sub_credit" required class="input"><br>
						   <button type="submit" class="btn" name="submit">Add Subject Details</button>
						</form>
				
				
					</div>
				
				
				<div class="tbox" >
					<h3 style="margin-top:30px;"> Subject Details</h3><br>
					<?php
						if(isset($_GET["mes"]))
						{
							echo"<div class='error'>{$_GET["mes"]}</div>";	
						}
					
					?>
					<table border="1px" >
						<tr>
							<th>S.code</th>
							<th>Subject Name</th>
							<th>Subject Credit</th>
							<th>Delete</th>
						</tr>
						<?php
							$s="select * from sub";
							$res=$db->query($s);
							if($res->num_rows>0)
							{
								
								while($r=$res->fetch_assoc())
								{
									
									echo "
										<tr>
										<td>{$r["sub_code"]}</td>
										<td>{$r["sub_name"]}</td>
										<td>{$r["sub_credit"]}</td>
										<td><a href='sub_delete.php?id={$r["sub_code"]}' class='btnr'>Delete</a></td>
										</tr>
									
									";
									
								}
								
							}
							else
							{
								echo "No Record Found";
							}
						?>
						
					</table>
				</div>
			</div>
	
		
	</body>
</html>