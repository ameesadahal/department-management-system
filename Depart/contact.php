<?php
	include "database.php";
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Contact</title>
		<link rel="stylesheet" type="text/css" href="css/styles.css">
	</head>
	<body class="back">
		<?php include"navbar.php";?>
		<img src="img/cc.jpg" width="800">
		
		<div class="login">
			<h1 class="heading">Contact Us</h1>
			<div class="cont">
			
				<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
				
					Department of Computer Science and Engineering,<BR>
					CS 2nd Year/ 2nd Sem<BR>
					Saugat Shakya<BR>
					Amisha Dahal<BR>
					Minusha Gurung<BR>
					Prarthana Prasai<BR>
				</form>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		 <script>
		$(document).ready(function(){
			$(".error").fadeTo(1000, 100).slideUp(1000, function(){
					$(".error").slideUp(1000);
			});
			
			$(".success").fadeTo(1000, 100).slideUp(1000, function(){
					$(".success").slideUp(1000);
			});
		});
	</script>
		
	
		
	</body>
</html>