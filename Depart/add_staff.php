<?php
	include"database.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
		echo"<script>window.open('index.php?mes=Access Denied...','_self');</script>";
		
	}	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Add Staff</title>
		<link rel="stylesheet" type="text/css" href="css/styled.css">
	</head>
	
	<body>
			<?php include"navbar.php";?><br>
			<img src="img/cc.jpg" style="margin-left:90px;" class="sha">
			
			<div id="section">
				
				<?php include"sidebar.php";?><br><br><br>
				
				<h3 class="text">Welcome <?php echo $_SESSION["ANAME"]; ?></h3><br><hr><br>
				<div class="content1">
					
						<h3 > Add Staff Details</h3><br>
						
					<?php
						if(isset($_POST["submit"]))
						{
							$sq="insert into staff(ID,NAME,GENDER,TYPE,QUAL,POS,SAL,PNO,MAIL,ADDR) values('{$_POST["ID"]}','{$_POST["NAME"]}','{$_POST["GENDER"]}','{$_POST["TYPE"]}','{$_POST["QUAL"]}','{$_POST["POS"]}','{$_POST["SAL"]}','{$_POST["PHO"]}','{$_POST["MAIL"]}','{$_POST["ADDR"]}')";
							if($db->query($sq))
							{
								echo "<div class='success'>Insert Success..</div>";
							}
							else
							{
								echo "<div class='error'>Insert Failed..</div>";
							}
							
						}
						
					?>
					<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
					<div class="lbox">
						 <label>Staff Id</label><br>
					     <input type="number" name="ID" required class="input3">
						 <br><br>
						 <label>Staff Name</label><br>
					     <input type="text" name="NAME" required class="input3">
					     <br><br>
						 <label>Gender</label>
					<select name="GENDER" required class="input3">
							<option value="">Select</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
					</select><br><br>
						
					     
					</div>
					
					<div  class="rbox">
					<label>Staff Type</label><br>
					     <select name="TYPE" required class="input">
						 <option value="">Select</option>
						 <option value="Faculty">Faculty</option>
						 <option value="Non-Faculty">Non-Faculty</option>
						 </select>
						 <br><br>
					<label>Qualification</label><br>
					     <input type="text" name="QUAL" required class="input">
					     <br><br>
						 <label>Position</label><br>
					     <input type="text" name="POS" required class="input">
					     <br><br>
					     <label>Salary</label><br>
					     <input type="number" name="SAL" required class="input">
					     <br><br>

					</div>

					<div class="pbox">
					<label> Phone No</label><br>	
						<input type="text" class="input" name="PHO"><br><br>
						 <label> Mail Id</label><br>
						 <input type="email" class="input" name="MAIL"><br><br>
				    	 <label>  Address</label><br>
					     <textarea rows="3" name="ADDR"></textarea><br><br>
					     <button type="submit" class="btn" name="submit">Add Staff Details</button>
					</div>
					</form>
				
				
				</div>
				<div class="tbox">
					<h3 style="margin-top:30px;"> Staff Details</h3><br>
					<?php
						if(isset($_GET["mes"]))
						{
							echo"<div class='error'>{$_GET["mes"]}</div>";	
						}
					
					?>
					<table border="1px" >
						<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Gender</th>
						<th>Type</th>
						<th>Qualification</th>
						<th>Position</th>
						<th>Salary</th>
						<th>Phone</th>
						<th>Mail</th>
						<th>Address</th>
						<th>Delete</th>
						</tr>
						<?php
							$s="select * from staff";
							$res=$db->query($s);
							if($res->num_rows>0)
							{
								while($r=$res->fetch_assoc())
								{
									echo "
										<tr>
										<td>{$r["ID"]}</td>
										<td>{$r["NAME"]}</td>
										<td>{$r["GENDER"]}</td>
										<td>{$r["TYPE"]}</td>
										<td>{$r["QUAL"]}</td>
										<td>{$r["POS"]}</td>
										<td>{$r["SAL"]}</td>
										<td>{$r["PNO"]}</td>
										<td>{$r["MAIL"]}</td>
										<td>{$r["ADDR"]}</td>
										<td><a href='staff_delete.php?ID={$r["ID"]}' class='btnr'>Delete</a><td>

										</tr>
										";
									
								}
								
							}
						?>
					
					</table>
				</div>
				
				
			</div>
	
	</body>
</html>