<?php
	include"database.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
		echo"<script>window.open('index.php?mes=Access Denied...','_self');</script>";
		
	}	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Student</title>
		<link rel="stylesheet" type="text/css" href="css/styled.css">
	</head>
	<body>
		<?php include"navbar.php";?><br>
		
			<div id="section">
				<?php include"sidebar.php";?><br><br><br>
				<h3 class="text">Welcome <?php echo $_SESSION["ANAME"]; ?></h3><br><hr><br>
				<div class="content">
					
						<h3 >Add Student Details</h3><br>
					<?php
						if(isset($_POST["submit"]))
						{
							$edate=$_POST["da"].'-'.$_POST["mo"].'-'.$_POST["ye"];
							$target="student/";
							$sq="insert into student(r_no,name,age,DOB,GEN,PHO,MAIL,ADDR,p_name,semester) values('{$_POST["r_no"]}','{$_POST["name"]}','{$_POST["age"]}','{$edate}','{$_POST["gen"]}','{$_POST["pho"]}','{$_POST["email"]}','{$_POST["addr"]}','{$_POST["p_name"]}','{$_POST["semester"]}')";
								
								if($db->query($sq))
								{
									echo "<div class='success'>Insert Success</div>";
								}
								else
								{
									echo "<div class='error'>Insert Failed</div>";
								}
							
							
						}
					
					
					
					
					
					
					?>
			
				<form method="post" enctype="multipart/form-data" action="<?php echo $_SERVER["PHP_SELF"];?>">
				<div class="lbox">
					<label> Registration number</label><br>
					<input type="number" class="input3" name="r_no" style="background:#b1b1b1;" required><br><br>
					<label> Student Name</label><br>
					<input type="text" class="input3" name="name"><br><br>
					
				
						
					<label>  Date of Birth</label><br>
					<select name="da" class="input5" required>
						<option value="">Date</option>
						<option value="1">1 </option>
						<option value="2">2 </option>
						<option value="3">3 </option>
						<option value="4">4 </option>
						<option value="5">5 </option>
						<option value="6">6 </option>
						<option value="7">7 </option>
						<option value="8">8 </option>
						<option value="9">9 </option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						</select>
					<select name="mo" class="input5" required>
						<option> Month</option>
						<option value="01">Jan</option>
						<option value="02">Feb</option>
						<option value="03">Mar</option>
						<option value="04">Apr</option>
						<option value="05">May</option>
						<option value="06">Jun</option>
						<option value="07">Jul</option>
						<option value="08">Aug</option>
						<option value="09">Sep</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>
					</select>
					<select name="ye" class="input5">
		<option value="">Year</option>
	        <?php
		for($i=1900;$i<=2014;$i++)
		{
		echo "<option value='$i'>$i</option>";
		}
		?>
	</select><br><br>
					<label>Gender</label>
					<select name="gen" required class="input3">
							<option value="">Select</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
					</select><br><br>

					<label> age</label><br>
					<input type="number" name="age" class="input3"><br><br>
					
					
				</div>
				
				<div class="rbox">
				<label> Phone No</label><br>
					<input type="number" class="input3" name="pho"><br><br>
				
				<label> Mail Id</label><br>
					<input type="email" class="input3" name="email"><br><br>
					
					<label>  Address</label><br>
					<textarea rows="3" name="addr"></textarea><br><br>
				
					<label>Program </label><br>
					<select name="p_name" required class="input3">
				
						<?php 
							 $sl="SELECT DISTINCT(name) FROM program";
							$r=$db->query($sl);
								if($r->num_rows>0)
									{
										echo"<option value=''>Select</option>";
										while($ro=$r->fetch_assoc())
										{
											echo "<option value='{$ro["name"]}'>{$ro["name"]}</option>";
										}
									}
						?>
					
					</select>
					<br><br>
						<label>semesters</label><br>
						<input type="number" name="semester" class="input3" required>
							 <br></br>
			
			<button type="submit" style="float:right;" class="btn" name="submit">Add Student Details</button>
				</div>
					
				</form>
				
				
				</div>
				<div class="tbox">
					<h3 style="margin-top:30px;"> Student Details</h3><br>
					<?php
						if(isset($_GET["mes"]))
						{
							echo"<div class='error'>{$_GET["mes"]}</div>";	
						}
					
					?>
					<table border="1px" >
						<tr>
						<th>Registration number</th>
						<th>Name</th>
						<th>Age</th>
						<th>DOB</th>
						<th>Gender</th>
						<th>Phone</th>
						<th>Mail</th>
						<th>Address</th>
						<th>Program</th>
						<th>Semester</th>
						<th>Delete</th>
						</tr>
						<?php
							$s="select * from student";
							$res=$db->query($s);
							if($res->num_rows>0)
							{
								while($r=$res->fetch_assoc())
								{
									echo "
										<tr>
										<td>{$r["r_no"]}</td>
										<td>{$r["name"]}</td>
										<td>{$r["age"]}</td>
										<td>{$r["DOB"]}</td>
										<td>{$r["GEN"]}</td>
										<td>{$r["PHO"]}</td>
										<td>{$r["MAIL"]}</td>
										<td>{$r["ADDR"]}</td>
										<td>{$r["p_name"]}</td>
										<td>{$r["semester"]}</td>
										<td><a href='stud_delete.php?r_no={$r["r_no"]}' class='btnr'>Delete</a><td>
										</tr>
										";
									
								}
								
							}
						?>
					
					</table>
				</div>
				
			</div>
	
				
	</body>
</html>