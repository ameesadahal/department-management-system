<?php
	include"database.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
		echo"<script>window.open('index.php?mes=Access Denied...','_self');</script>";
		
	}	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Class</title>
		<link rel="stylesheet" type="text/css" href="css/styled.css">
	</head>
	<body>
		<?php include"navbar.php";?><br>
		<img src="img/coding-cpp.png" style="margin-left:90px;" class="sha">
			<div id="section">
				<?php include"sidebar.php";?><br>
				<h3 class="text">Welcome <?php echo $_SESSION["ANAME"]; ?></h3><br><hr><br>
				<div class="content1">
					
						<h3 > Add Room Details</h3><br>
					<?php
						if(isset($_POST["submit"]))
						{
							 $sq="insert into class(Room_no,C_type) values('{$_POST["Room_no"]}','{$_POST["C_type"]}')";
							if($db->query($sq))
							{
								echo "<div class='success'>Insert Success..</div>";
							}
							else
							{
								echo "<div class='error'>Insert failed..</div>";
							}
							
							
						}
					
					?>
						
				<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
					<label>Room no:</label><br>
						<input type="number" name="Room_no" class="input2" required>
					<br><br>
					<label>Type</label><br>
					<select name="C_type"  required class="input2">
						<option value="">Select</option>
						<option value="lab">lab</option>
						<option value="classroom">classroom</option>
						<option value="Teacher room">Teacher room</option>
						<option value="HOD room">HOD room</option>
						<option value="Non Faculty room">Non Faculty room</option>
						<option value="Dean room">Dean room</option>
						<option value="Assistant Dean room">Assistant Dean room</option>
						<option value="Mechanical Room">Mechanical room</option>
						<option value="Assistant prof Room">Assistant prof room</option>
						<option value="Adminstration Room">Adminstration room</option>
						<option value="Graduation Room">Graduation room</option>
						<option value="Maintenance Room">Maintenance room</option>
					
					</select>
					<br>
					<button type="submit" class="btn" name="submit">Add Room Details</button>
				</form>
				
				
				</div>
				
				
				<div class="tbox">
					<h3 style="margin-top:30px;"> Room Details</h3><br>
					<?php
						if(isset($_GET["mes"]))
						{
							echo"<div class='error'>{$_GET["mes"]}</div>";	
						}
					
					?>
					<table border="1px" >
						<tr>
							<th>Room No</th>
							<th>Class Type</th>
							<th>Delete</th>
						</tr>
						<?php
							$s="select * from class";
							$res=$db->query($s);
							if($res->num_rows>0)
							{
								while($r=$res->fetch_assoc())
								{
									echo "
										<tr>
											<td>{$r["Room_no"]}</td>
											<td>{$r["C_type"]}</td>
											<td><a href='delete.php?id={$r["Room_no"]}' class='btnr'>Delete</a></td>
										</tr>
										";
									
								}
								
							}
						?>
					
					</table>
				</div>
			</div>
	
	</body>
</html>